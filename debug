#!/usr/bin/fish

function help
  echo "Usage: debug [-q] [-v VERBOSITY] [-t TRACE] [--] FISH_ARGUMENTS..."
  echo "Options:"
  echo "  -q --quiet"
  echo "  -t --trace"
  echo "  -v --verbose"
end

function debug--regression-test
  argparse 'n/iterations=' 'threshold=' 'interval=' 'profile-dir=' -- $argv

  set branch ( git branch --show-current )

  set -e commit_a
  set -e commit_b

  if set -q argv[2]
    set commit_a $argv[1]
    set commit_b $argv[2]
    set argv $argv[3..]
  else if set -q argv[1]
    set commit_a $argv[1]
  else
    set commit_a $branch~1
  end
  
  if not set -q _flag_profile_dir
    set profile_dir ( mktemp -d "/tmp/tictafish-debug-regression-test.XXXXXX" )
  else
    set profile_dir "$_flag_profile_dir"
  end

  if not set -q _flag_iterations
    set test_iterations 10
  else
    set test_iterations "$_flag_iterations"
  end

  if not set -q _flag_interval
    set test_interval 0.5
  else
    set test_interval "$_flag_interval"
  end
  
  set test_iterations_midpoint ( math "max(1, round($test_iterations / 2))" )
  set test_seq ( seq $test_iterations )
  
  echo "iterations: $test_iterations"
  if set -q _flag_threshold
    echo "threshold: $_flag_threshold%"
  end
  echo
  if set -q commit_a
    echo "commit_a: $commit_a"
  else
    echo "commit_a: HEAD (no checkout)"
  end
  if set -q commit_b
    echo "commit_b: $commit_b"
  else
    echo "commit_b: HEAD (no checkout)"
  end
  echo

  cd $ticta__dir

  set test_benchmark_command 'fish -ilc "exit # tictafish performance regression test"'

  for i in $test_seq
    # profile previous commit to current branch
    ./debug -q -t 0 -v 0 --checkout "$commit_a" -- -il --profile "$profile_dir/profile_previous.$i.txt" -c "$test_benchmark_command" &> /dev/null
    or return $status
    
    # profile current working tree, even if it's not committed
    ./debug -q -t 0 -v 0 -- -il --profile "$profile_dir/profile_head.$i.txt" -c "$test_benchmark_command" &> /dev/null
    or return $status

    sleep $test_interval
    or return 130
  end

  set cumulative_previous
  set cumulative_head 

  for i in $test_seq
    set lines_previous ( cat "$profile_dir/profile_previous.$i.txt" )
    set lines_head     ( cat "$profile_dir/profile_head.$i.txt" )
    
    set value_previous ( string match --regex "^.+> $test_benchmark_command" -- $lines_previous | string split -f 2 \t )
    set value_head     ( string match --regex "^.+> $test_benchmark_command" -- $lines_head     | string split -f 2 \t )
    
    set -a cumulative_previous ( math "$value_previous / 1000" )  
    set -a cumulative_head     ( math "$value_head / 1000" )    
  end

  set cumulative_previous__sorted ( string join \n -- $cumulative_previous | sort - )
  set cumulative_head__sorted     ( string join \n -- $cumulative_head | sort - )

  set median_previous $cumulative_previous__sorted[$test_iterations_midpoint]
  set median_head     $cumulative_head__sorted[$test_iterations_midpoint]

  set gain ( math "$median_previous - $median_head")
  set gain_percent ( math "( $gain / $median_previous ) * 100" )

  echo "cumulative time:"
  echo "  previous: ["( string replace --regex '(.*)' '$1ms' -- $cumulative_previous__sorted | string join ", " )"]"
  echo "  previous_median: $median_previous""ms"
  echo "  head: ["( string replace --regex '(.*)' '$1ms' -- $cumulative_head__sorted | string join ", " )"]"
  echo "  head_median: $median_head""ms"
  echo
  echo "diff:"
  echo "  performance_gain: $gain""ms"
  echo "  performance_gain_percent: $gain_percent%"

  if set -q _flag_loss_threshold && test "$gain_percent" -le "$loss_threshold"
    exit 1
  end

  exit 0
end

function debug--fish
  set -l tmp ( mktemp -d /tmp/tictafish-tmpdir.XXXXXXX )
  mkdir $tmp/{run,tmp,tmux}
  set -x TICTA_RUNTIME_DIR $tmp/run
  set -x TMPDIR $tmp/tmp
  set -x TMUX_TMPDIR $tmp/tmux
  tmux -qc 'exit' &> /dev/null # Creates a new socket
  
  set -l path
  if not set -q _flag_checkout
    set path ( status current-filename | path resolve | path dirname )"/ticta.fish"
  else
    git clone ( status current-filename | path resolve | path dirname ) "$tmp/tictafish" &> /dev/null
    or return $status

    git -C $tmp/tictafish checkout $_flag_checkout &> /dev/null
    or return $status

    set path $tmp/tictafish/ticta.fish
  end

  set -x TICTA_DEBUG 1

  if not set -q _flag_trace
    set -x TICTA_TRACE 1
  else
    set -x TICTA_TRACE "$_flag_trace"
  end

  if not set -q _flag_verbosity
    set -x TICTA_VERBOSITY 3
  else
    set -x TICTA_VERBOSITY "$_flag_verbosity"
  end

  set -x TICTA_DISABLED 1
  fish -NC "set -e TICTA_DISABLED && source $path" $argv
end

function main
  set verb "fish"
  set -e _flag_trace
  set -e _flag_verbosity
  set -e _flag_quiet

  while set -q argv[1]
    switch $argv[1]
      case "-h" "--help"
        help
        return
        
      case "-q" "--quiet"
        set -x _flag_quiet
        set argv $argv[2..]

      case "-v" "--verbosity"
        set -x _flag_verbosity $argv[2]
        set argv $argv[3..]

      case "-t" "--trace"
        set -x _flag_trace $argv[2]
        set argv $argv[3..] 

      case "--checkout"
        set -x _flag_checkout $argv[2]
        set argv $argv[3..]

      case "regression-test"
        set verb $argv[1]
        set argv $argv[2..]

      case "--"
        set argv $argv[2..]
        break

      case "*"
        break
    end
  end

  switch "$verb"
    case "fish"
      debug--fish $argv
    case "regression-test"
      debug--regression-test $argv
    case "*"
      echo "erm, awkward. You shouldn't be seeing this. our monkeyz are totally on it!!! 😜😜😜"
  end 
end

main $argv
complete -x -c "ticta--function-is-number"
function ticta--function-is-number -a x
  test "$x" -eq 0 -o "$x" -ne 0 &> /dev/null; and return 0
  return 1
end
tictabox-register is-number -d "Asserts if input is a number or not"

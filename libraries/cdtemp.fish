# provides cdtemp Creates a dir with mktemp then moves into it

complete -x -c ticta--lib-function-cdtemp
function ticta--lib-function-cdtemp
  set tmp ( mktemp -t -d cdtemp.XXXXXX )
  cd $tmp
  echo $tmp
end

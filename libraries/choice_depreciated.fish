# provides choice_list (Depreciated)
# provides choice_str (Depreciated) Present user with choice of string
# provides choice_yesno (Depreciated)

function ticta--lib-function-choice_list
  function print
    echo $argv > /dev/stdin
  end

  argparse "p/prompt=" "d/default=" "c/choice=+" -- $argv

  if [ -z $_flag_default ]
    set _flag_default 1
  end

  set c_choice_i   ( set_color brred )
  set c_choice_key ( set_color green )
  print $_flag_prompt

  set -l i 1
  for arg in $_flag_choice
    print $c_choice_i"["$i"]"$normal $arg
    set i (expr $i + 1)
  end
  print $c_choice_i"[... Or type in your own]"
  set input ( read -P "Number or custom value (def: "$c_choice_i$_flag_default$normal")> " )

  if test -z $input
    echo $_flag_choice[$_flag_default]
  else if test $input -gt (count $_flag_choice) 2> /dev/null
       or test $input -lt 1 2> /dev/null
    echo Invalid index > /dev/stderr
    echo ""
    return 1
  else if test $input -gt 0 2> /dev/null
    echo $_flag_choice[$input]
  else
    echo $input
  end
end

complete -x -c ticta--lib-function-choice_str -s 's' -l 'prompt' -d 'Prompt text'
complete -x -c ticta--lib-function-choice_str -s 'd' -l 'default' -d 'Default input if user enters nothing'
complete -x -c ticta--lib-function-choice_str -s 'r' -l 'required' -d 'Makes prompt mandatory, empty input not allowed'

function ticta--lib-function-choice_str
  function print
    echo -ne $argv > /dev/stdin
  end

  argparse "p/prompt=" "d/default=" "r/required" -- $argv

  function _prompt -S
    if not set -q _flag_prompt
      set _flag_prompt "Enter string"
    end

    set prompt
    set -a prompt $_flag_prompt
    if set -q _flag_default; and [ -n $_flag_default ]
      set -a prompt " [$_flag_default]"
    end
    set -a prompt ": "

    # print "Required?"
    # print (set -q _flag_required; and echo yes; or echo no) $_flag_required \n
    read -P ( string join "" $prompt ) res
    # print ( string length $res ) ( [ -z $res ]; and echo "zero"; or echo "nonzero")
    if [ -n $res ]
      echo $res
    else
      if set -q _flag_required
        esc up 1
        esc clearline
        print \r
        print "Input required :("\n
        _prompt
      else
        echo $_flag_default
      end
    end
  end
  _prompt
end

function ticta--lib-function-choice_yesno
  argparse "p/prompt=" "d/default=" "e/end=" "l/loud" -- $argv

  if [ -z $_flag_prompt ]
    set _flag_prompt "Yes or no?"
  end
  if [ -z $_flag_default ]
    set _flag_default yes
  end
  if [ -z $_flag_end ]
    set _flag_end " > "
  end

  # getopts $argv | while read -l key value
  #   # echo $key : $value
  #   switch $key
  #   case p prompt
  #     set prompt $value
  #   case d default
  #     if [ $value = "no" ]
  #       set default 1
  #     end
  #   case e "end"
  #     set cap $value
  #   end
  # end

  set -l optsStr "[Y]/n"
  if [ $_flag_default = "no" ]
    set optsStr "y/[N]"
  end

  set choice ( string lower ( read -P "$_flag_prompt $optsStr$_flag_end" ) )

  function print
    if set -q _flag_loud; and [ $_flag_loud -gt 0 ]
      echo $argv
    end
  end

  switch (echo $choice)
  case ""
    if [ $_flag_default = "no" ]
      print n
      return 1
    else if [ $_flag_default = "yes" ]
      print y
      return 0
    end
  case y yes
    print y
    return 0
  case *
    print n
    return 1
  end
end

# provides regex-rename Rename files with regular expressions!
# provides regex-copy Copies files with regular expressions! (alias of regex-rename --copy)
# provides permission-calculator
# provides wifi-signal-monitor Monitor wireless interface signal strengths

function ticta--lib-function-regex-rename
  $ticta__dir/scripts/regex-rename.py $argv
end

function ticta--lib-function-regex-copy
  $ticta__dir/scripts/regex-rename.py --copy $argv
end


complete -x -c ticta--lib-function-regex-rename -s i -l match-directories -d "Allow matching and renaming/copying directories."
complete -x -c ticta--lib-function-regex-rename -s e -l regex-match       -d "Use a regex initial file match instead of glob"
complete -x -c ticta--lib-function-regex-rename -s r -l recursive         -d "Allow recursive file matching, (e.g. **/*.js)"
complete -x -c ticta--lib-function-regex-rename -s y -l yes               -d "Don't confirm before renaming"
complete -x -c ticta--lib-function-regex-rename -s c -l copy              -d "Copy instead of moving files"
complete -x -c ticta--lib-function-regex-rename -s p -l hide-progress     -d "Don't print progress bar"
complete -x -c ticta--lib-function-regex-rename -s i -l case-insensitive  -d "Case insensitive"

complete -x ticta--lib-function-regex-copy -w ticta--lib-function-regex-rename

function ticta--lib-function-permission-calculator
  $ticta__dir/scripts/permission-calculator.py
end

function ticta--lib-function-wifi-signal-monitor
  $ticta__dir/scripts/wifi-signal-monitor.pl $argv
end

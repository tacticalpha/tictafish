# provides format
# provides format-string

complete -x -c ticta--lib-function-format -a "bool number color" -n "not __fish_seen_subcommand_from bool number color"
function ticta--lib-function-format
  set color_names {,br}{black,white,red,green,yellow,blue,magenta,cyan}

  argparse 'normal=' 'color-block-left' 'default-to-string' -- $argv

  set type $argv[1]
  set value $argv[2..]

  if set -q _flag_normal
    set normal $_flag_normal
  else
    set normal ( set_color normal )
  end

  set output

  if test "$type" = "auto"
    if set -q value[2]
      set type "list"
    else if string match --quiet --regex -i '^(t|true|y|yes|f|false|n|no)$' -- "$value"
      set type "bool"
    else if string match --quiet --regex -i '^('( string join "|" -- $color_names )'|((0x|#)[0-9a-f]{3,6})|(hsl\(.+\)))$' -- "$value"
      set type "color"
    else if string match --quiet --regex '^[0-9]+(.[0-9]+)?$' -- "$value"
      set type "number"
    else if string match --quiet --regex '^((\'.+\')|(".+"))$' -- "$value"
      set type "string"
    else if set -q _flag_default_to_string
      set type "string"
    else
      set type "unknown"
    end
  end

  switch $type
  case "bool"
    if tictabox assert-bool "$value"
      set output "$bold$brgreen$value$unbold$normal"
    else
      set output "$bold$brred$value$unbold$normal"
    end    

  case "color"
    if test -n "$_flag_color_block_left"
      # set output ""( ticta-set-color $value )"███$normal $value "
      set output ""( ticta--resolve-color --set $value )"██$normal $value "
    else
      # set output "$value "( ticta-set-color $value )"███$normal"
      set output "$value "( ticta--resolve-color --set $value )"██$normal"
    end

  case "int"
    set output "$brblue$value$normal" 

  case "list"
    if not count $value > /dev/null
      set output "[]"
    else
      for flag in _flag_color_block_left _flag_default_to_string
        if set -q $flag
          set -x $flag $$flag
        end
      end

      set list
      for item in $value
        if set formatted ( ticta--lib-function-format --default-to-string auto "$item" )
          set item "$formatted"
        end
        if string match --quiet ',' -- "$item"
          set item "$normal$dim\"$normal""$item""$normal$dim\"$normal"
        end
        set -a list "$item"
      end
      
      set output "["( string join "$normal""$dim"", ""$dim_off""$normal" -- $list )"]"
    end

  case "list_plain"
    if not count $value > /dev/null
      set output "[]"
    else      
      set output "["( string join "$normal""$dim"", ""$dim_off""$normal" -- $value )"]"
    end

  case "number" "float"
    set output "$brblue"( string split '.' -- "$value" | string join "$dim.$dim_off" )

  case "scheme"
    set output
    for color_name in $color_list
      if set color ( ticta--resolve-color --set --scheme "$value" "$color_name" )
        set output "$output$color█$normal"
      else
        set output "$output$normal?"
      end
    end

    if set -q _flag_color_block_left
      set output "$output $value"
    else
      set output "$value $output"
    end

  case "string"
    set value ( string trim --char '\'"' -- "$value" )
    set output "$dim\"$dim_off$yellow$value$normal$dim\"$dim_off"

  case "unknown" "*"
    # ticta--warn "unknown format type: '$type'"
    set output "$value"
    # if test -z "$_flag_allow_unknown"
    #   return 1
    # end
  end

  echo -en "$normal""$output""$normal"
end

# set -l ticta__format_regexp 'f{(?<type>\w+):(?<content>.+)}'
# set -g ticta__format_string_regexp "(?<format_target>$ticta__format_regexp)|(?<remainder>\w+|\s+)"
# function ticta--lib-function-format-string
#   if contains -- "$argv[1]" "-h" "--help"
#     echo "Usage: ticta-lib format-string <input>" > /dev/stderr
#     echo "  Replaces all templates with the following format in the above string, using ticta-lib function 'format'." > /dev/stderr
#     echo "  'f{<type>:<value>}'" > /dev/stderr
#     echo "Example: " > /dev/stderr
#     echo "  ticta-lib format-string \"Command returned f{bool:false} (code: f{int:42})\"" > /dev/stderr
#     return 1
#   end
#   string match --all --regex $ticta__format_string_regexp -- "$argv" > /dev/null

#   for i in ( seq ( count $format_target ) )
#     if test -n "$format_target[$i]"
#       echo -n ( ticta--lib-function-format $type[$i] $content[$i] )
#     else if test -n "$remainder[$i]"
#       echo -n "$remainder[$i]"
#     end
#   end
# end

begin
  set -l formats '\<(?:(?<format_type>\w+):)?(?<format_target>.+)\>'
  set -a formats '\*(?<bold_content>.+)\*'
  set -a formats '\`(?<code_content>.+)\`'
  set -a formats '(?<remainder>[^\<\*\`]+)' 
  set -g ticta__format_string_regexp2 ( string replace --regex '(.*)' '(?:$1)' -- $formats | string join '|' )
end
function ticta--lib-function-format-string
  if contains -- "$argv[1]" "-h" "--help"
    echo "Usage: ticta-lib format-string <input>" > /dev/stderr
    echo "  Replaces all templates with the following format in the above string, using ticta-lib function 'format'." > /dev/stderr
    echo "  '<<type>:<value>>'" > /dev/stderr
    echo "Example: " > /dev/stderr
    echo "  ticta-lib format-string \"Command returned <bool:false} (code: f{int:42>)\"" > /dev/stderr
    return 1
  end
  string match --all --regex $ticta__format_string_regexp2 -- "$argv" > /dev/null

  set output ""

  for i in ( seq ( count $format_target ) )

    if test -n "$format_target[$i]"
      if test -n "$format_type[$i]"
        set -a output ( ticta--lib-function-format $type[$i] $format_target[$i] )
      else
        set -a output ( ticta--lib-function-format auto $format_target[$i] )
      end

    else if test -n "$bold_content[$i]"
      set -a output "$bold""$bold_content[$i]""$bold_off"

    else if test -n "$code_content[$i]"
      set -a output "$bg_brblack$brred$bold`$normal$bg_brblack$brwhite""$code_content[$i]""$brred$bold`"
      
    else if test -n "$remainder[$i]"
      set -a output "$remainder[$i]"
    end

    set -a output "$normal"
  end

  string join "" -- $output
end

begin
  set -l formats '**' '$bold$content$bold_off'
  set -a formats '<>' '$( ticta--lib-function-format auto "$content" )'

  set -g ticta__format_string__patterns
  set -g ticta__format_string__replacements
  while set -q formats[1]
    set delimiters ( string split "" $formats[1] )
    set replacement $formats[2]
    set formats $formats[3..]
    
    set delimit_start $delimiters[1]
    if not set -q delimiters[2]
      set delimit_end $delimit_start
    else
      set delimit_end $delimiters[2]
    end

    set -ga ticta__format_string__patterns '(?<matches>(?<matches_pre>(?:^|[^\\\\]|\\\\\\\\))\\'"$delimit_start"'(?<matches_content>.*(?:^|[^\\\\]|\\\\\\\\))\\'"$delimit_end"')'
    set -ga ticta__format_string__replacements "$replacement"
  end
  # set -l ticta__format_regexp '<(?<type>\w+):(?<content>.+)>'
  # set -g ticta__format_string_regexp "(?<format_target>$ticta__format_regexp)|(?<remainder>\w+|\s+)"
end
ticta--debug -v ticta__format_string__patterns ticta__format_string__replacements
function ticta--lib-function-format-string2
  if contains -- "$argv[1]" "-h" "--help"
    echo "Usage: ticta-lib format-string <input>" > /dev/stderr
    echo "  Replaces all templates with the following format in the above string, using ticta-lib function 'format'." > /dev/stderr
    echo "  '<<type>:<value>>'" > /dev/stderr
    echo "Example: " > /dev/stderr
    echo "  ticta-lib format-string \"Command returned <bool:false} (code: f{int:42>)\"" > /dev/stderr
    return 1
  end
  set output "$argv"

  set i 1
  for pattern in $ticta__format_string__patterns
    set replacement_template $ticta__format_string__replacements[$i]
    # ticta--debug -v i pattern replacement_template
    
    string match --quiet --all  --regex "$pattern" "$output"
    for var in i pattern replacement_template matches
      ticta--debug "$var = '$$var'"
    end
    set i ( math "$i + 1" )

    while set -q matches[1]
      set match $matches[1]
      set pre $matches_pre[1]
      set content $matches_content[1]

      set replacement ( eval "echo $replacement_template" )
      ticta--debug "replacement eval: \"echo $replacement_template\""
      for var in i match pre content replacement
        ticta--debug "$var = '$$var'"
      end

      set output ( string replace "$match" "$pre$replacement" "$output" )

      set matches $matches[2..]
      set matches_pre $matches_pre[2..]
      set matches_content $matches_content[2..]
    end
  end

  echo $output
end
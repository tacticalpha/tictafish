# provides kv

set fish_var_regexp '[\w_]+'

function ticta--lib-function-kv--help
  echo "Usage: kv [OPTIONS] [READ_VAR [WRITE_VAR]]  [ACTIONS]"
  echo "  If called with no actions, --print is implied."
  echo "Options:"
  echo "  -p/--print   Pretty-print data"
  echo "  -q/--quiet   No stdout output. Overrides --print"
  echo "  -i/--inline  When creating a new list, set it to inline. If list already"
  echo "               exists, this will reset it"
  echo "Arguments:"
  echo "  READ_VAR   Variable to read data from"
  echo "  WRITE_VAR  If READ_VAR is set, alternative var to write data to"
  echo "             without updating READ_VAR"
  echo "Actions:"
  echo "  --set KEY VALUE  Sets KEY to VALUE"
  echo "  --get [VAR=]KEY  Saves key to var of the same name or VAR specified by"
  echo "                   VAR=... Ex. `--get kv_val=x` saves the key `x` to the"
  echo "                   variable `kv_val`. Just --get x would save `x` to `x`"
  echo "  --erase [KEY]    Unsets KEY"
  echo "  --keys[=VAR]     If VAR is specified, saves current list of keys to $VAR."
  echo "                   Otherwise outputs to stdout. This will inhibit piping."
  echo "  --dump[=PREFIX]  Saves all values to variables, optionally prefixed by PREFIX"
  echo "                   If PREFIX isn't set then 'key' is used. If there's an `=` but"
  echo "                   no value, no prefix will be used. Be careful about unintentionally"
  echo "                   overwriting variables."
  echo "  --assign TARGET  Similar to javascript's `Object.assign`. This merges the current kv"
  echo "                   with the one in $TARGET. Any values in $TARGET will be prioritized."
  echo "Default vs Inline Mode:"
  echo "  Default mode utilizes fish's built in list functionality to delimit pairs."
  echo "  This means it can't be added to a list, i.e. you can't make a list of"
  echo "  KV dictionaries. Inline mode is one long string, and can be added to fish lists,"
  echo "  but is slower to deserialize."
end

function ticta--lib-function-kv
  set -e flag_print
  set -e flag_quiet
  set -e flag_dirty
  set -e flag_inhibit_piping

  set -e flag_data_save_inline

  set -e read_var
  set -e write_var 

  set -e data
  set -e keys

  set escape_from '"'
  set escape_to   '\\"'

  while set -q argv[1]
    switch $argv[1]
      case "-h" "--help"
        ticta--lib-function-kv--help
        return
      case "-p" "--print"
        set flag_print
        set argv $argv[2..]
      case "-q" "--quiet"
        set flag_quiet
        set argv $argv[2..]
      case '-i' "--inline"
        set flag_data_save_inline
        set argv $argv[2..]
      case "*"
        break
    end
  end
  
  set var_regexp '^[^-]+$'
  if string match --quiet --regex "$var_regexp" -- $argv[1]
    set read_var ( string escape --style var -- $argv[1] )
    set write_var "$read_var"
    set argv $argv[2..]

    if string match --quiet --regex "$var_regexp" -- $argv[1]
      set write_var ( string escape --style var -- $argv[1] )
      set argv $argv[2..]
    end

    set data $$read_var
  end

  # Load current data
  begin
    if not test -t 0 # isatty stdin, but faster 
      while read line; set -a data "$line"; end
    end

    string match --quiet --regex --all '^(?:!(?<data_flags>\w*))?\s*(?<data>[\s\S]*)$' -- $data
    for flag in ( string split '' -- $data_flags e)
      switch "$flag"
        case "i"
          set flag_data_inline
      end
    end

    if not set -q flag_data_inline
      set data ( string split \n -- $data )
    end

    # this is pretty slow but it works for now
    if set -q flag_data_inline
      set regexp_global '((?:\w+)\s*=\s*(?<!\\\\)"(?:.*?)(?<!\\\\)")'
      set regexp_pair   '(?<key>\w+)\s*=\s*(?<!\\\\)"(?<value>.*?)(?<!\\\\)"'
      
      for pair in ( string match --all --regex --groups-only "$regexp_global" -- "$data" )
        string match --quiet --regex "$regexp_pair" "$pair" 
        or begin
          echo "error matching pair: status=$status pair='$pair' key='$key' value='$value'" > /dev/stderr
          continue
        end

        set value ( string replace --all --regex "$escape_to" "$escape_from" -- "$value" )
        
        if not contains "$key" $keys
          set -a keys "$key"
        end
        set key_$key "$value"
      end
    else
      for line in $data
        if test -z "$line"
          continue
        end
        
        string match --quiet --regex '^(?<key>\w+)\s*=\s*(?<value>.*)$' -- "$line  "
        or begin
          echo "error matching line: status=$status line='$line' key='$key' value='$value'" > /dev/stderr
          continue
        end

        set value ( string trim --right -- "$value" )

        if not contains "$key" $keys
          set -a keys "$key"
        end
        set key_$key "$value"
      end
    end
  end

  for key in $keys
    set -l var_value key_$key
  end

  if set -q argv[1]
    set flag_dirty
  end

  while set -q argv[1]
    switch $argv[1]
      case '-s' '--set'
        if not set -q argv[3]
          echo "error: too few arguments. '--set' requires two arguments." > /dev/stderr
          return 1
        end

        set key $argv[2]
        set value $argv[3]
        set argv $argv[4..]
        
        if not contains "$key" $keys
          set -a keys "$key"
        end
        set key_$key "$value"
        or return $status

      case '-g' '--get'
        if not set -q argv[2]
          echo "error: too few arguments. '--set' requires one argument." > /dev/stderr
          return 1
        end 

        set -e get_write_var
        string match --quiet --regex '^(?:(?<get_write_var>[\w_]+)=)?(?<key>[\w_]+)$' -- $argv[2]
        
        if test -z "$get_write_var"
          set get_write_var "$key"
        end
        
        set get_read_var "key_$key"

        # if set -ql $get_read_var
        #   # This is a little janky and I should probably just go through and
        #   # make sure all sets in here are local, but this will just explictly
        #   # set any local variable to local so the following `set -g` won't 
        #   # override it.
        #   set -l $get_read_var $$get_read_var
        # end

        set argv $argv[3..]

        if not contains "$key" $keys
          continue
        end

        set -g "$get_write_var" "$$get_read_var"

      case '-e' '--erase'
        if not set -q argv[2]
          echo "error: too few arguments. '--erase' requires one argument." > /dev/stderr
          return 1
        end
        
        set key $argv[2]
        set argv $argv[3..]

        for i in ( seq 1 ( count $keys ))
          if test "$key" != "$keys[$i]"
            continue
          end
          set -e keys[$i]
          set -e key_$key
          break
        end
      
      case "--keys" "--keys=*"
        set -e keys_write_var
        string match --quiet --regex -- '--keys(?:=(?<keys_write_var>\w+))?' $argv[1]
        
        set argv $argv[2..] 
        
        if test -z "$keys_write_var"
          set keys_write_var "keys"
        end

        set -g "$keys_write_var" $keys

      case "--dump" "--dump=*"
        set -e dump_prefix
        string match --quiet --regex -- '--dump(?<dump_prefix>=\w*)?' $argv[1]
        
        set argv $argv[2..]

        if test -z "$dump_prefix"
          set dump_prefix "key_"
        else if test "$dump_prefix" = "="
          set dump_prefix ""
        else
          string match --quiet --regex -- '=(?<dump_prefix>.*)' "$dump_prefix"
        end

        for key in $keys
          set -l read_var "key_$key"
          set -l write_var "$dump_prefix$key"
          set -g "$write_var" $$read_var
        end

      case '--assign'
        if not set -q argv[2]
          echo "error: too few arguments. '--assign' requires one argument." > /dev/stderr
          return 1
        end

        set target $argv[2]
        set argv $argv[3..]

        ticta-lib kv --quiet "$target" --keys=target_keys --dump=target_key_

        for key in $target_keys
          if not contains "$key" $keys
            set -a keys "$key"
          end
          set -l var_value "target_key_$key"
          set key_$key "$$var_value"
          or return $status
          echo set key_$key "\$$var_value"
        end

      case '*'
        echo "unknown argument: $argv[1]" > /dev/stderr
        return 1
    end
  end

  set output "!"
  if set -q flag_data_save_inline
    set output "$output""i"
  end
  
  for key in $keys
    set -l value_key "key_$key"
    
    if set -q flag_data_save_inline
      set -l value ( string replace --all "$escape_from" "$escape_to" -- "$$value_key" )
      set -a output "$key=\"$value\""
    else
      set -a output "$key=$$value_key"
    end
  end

  # assume if no actions were passed that the intention is to display
  #
  # this is kind of a mess. I don't know if I want to keep it but it works for now.
  # the issue is that `P; and Q; or R; and S` don't behave how they might in a
  # traditional language: (P and Q) or (R and S). 
  # It's evaluated as (((P and Q) or R) and S). 
  # We could just set each pair as variables and use test but whatever this works
  # for now.
  if begin
    #  stdout
    test -t 1; and not set -q flag_dirty
  end; or begin
    not set -q flag_quiet; and set -q flag_print
  end
    for key in $keys
      set -l value_key "key_$key"
      echo "$bold$brblue$key$normal = $brgreen$$value_key$normal" > /dev/stdout
    end
  end
    
  if set -q write_var
    if set -q flag_data_save_inline
      set -g "$write_var" "$output"
    else
      set output_lined
      for line in $output
        set output_lined "$output_lined$line"\n
      end
      set -g "$write_var" $output_lined
    end
  end

  #        stdout
  if not test -t 1; and not set -q flag_inhibit_piping
    string join \n -- $output
  end
end
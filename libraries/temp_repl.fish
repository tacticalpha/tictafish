# provides temp-repl

function ticta--lib-function-temp-repl -a extension editor
  cdtemp

  if test -n $extension
    set fn repl.$extension
  else
    set fn repl
  end
  touch $fn

  if test -n "$VISUAL"
    eval $VISUAL $fn
  else if test -n "$EDITOR"
    eval $EDITOR $fn
  else
    echo '$VISUAL and $EDITOR both not set :('
    ll
  end
end

complete -x -c ticta--function-temp-repl

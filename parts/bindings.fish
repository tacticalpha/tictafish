

set -ag ticta__bindings \
'pwd_history_back, '
# 'set_task, commandline --replace "set-task "( commandline --current-buffer ) && commandline -f execute, Interactively set dynamic window title task'
# 'help, ticta-bind list | more, Display all bindings' \

# set bindings ( string replace -ar ',\s+' ',' $bindings )

function ticta--bind-list
  argparse 'n/non-interactive' -- $argv

  set -l max_len 0
  set -l max_desc_len 0
  set -l max_bind_len 0

  for line in $bindings
    echo $line | read  -d ',' name line

    set -l len ( string length $name )
    test "$len" -gt "$max_len"; and set max_len $len

    set -l len ( string length $desc )
    test "$len" -gt "$max_desc_len"; and set max_desc_len $len

    set -l len ( ticta-cfg get keymap_bind_$name "" | string length | string escape )
    test "$len" -gt "$max_bind_len"; and set max_bind_len $len

  end

  function space_str -a target cur
    echo -n ( string repeat -n ( math $target - ( string length $cur ) + 1 ) ' ' )
  end

  for line in $bindings
    echo $line | read -d ',' name cmd desc

    set -l len ( string length $name )
    set -l bind_key "keymap_bind_$name"
    set -l binding ( ticta-cfg get $bind_key | string escape )
    set -l default_binding ( ticta-cfg get -d $bind_key | string escape )

    echo -n $name
    space_str $max_len $name
    # echo -n $desc
    # space_str $max_desc_len $desc
    echo -n $brblue$binding
    space_str $max_bind_len $binding
    echo " "$blue"["$default_binding"]"$normal
    echo " "$desc
  end

  set -e space_pad

  # string escape $output
end

function ticta--bind-help
  echo "hint: alt is \a (Alt+T -> \at), control is \c (Ctrl+T -> \ct)"
  echo "usage: ticta-bind list"
end


function ticta-bind
  echo $argv | read verb argv
  switch $verb
  case "list"
    ticta--bind-list
  case "*"
    ticta--bind-help
  end
end

set -l keys ( bind -K )
for line in $bindings
  echo $line | read -d ',' name cmd desc
  set -l binding ( ticta-cfg get "keymap_bind_$name" )
  if contains $binding $keys
    bind -k $binding "$cmd"
  else
    bind $binding "$cmd"
  end
end

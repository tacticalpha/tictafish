set -g color_list black red green yellow blue magenta cyan white
set -g full_color_list $color_list "br"$color_list

set -g ticta__named_colors
set -g named_colors $full_color_list \
'accent' \
'tertiary' \
'dark #1c1c1c'

for args in $named_colors
  echo $args | read name value

  set -a ticta__named_colors $name

  set -l varname "ticta__named_color_$name"
  if contains "$name" "accent" "tertiary"
    # This really should be completely redone, like the rest of the color stuff.
    # But this works for now.
    set -g $varname ( ticta-cfg get color_$name )
  else if test -z "$value"
    set -g $varname $name
  else
    set -g $varname $value
  end
end

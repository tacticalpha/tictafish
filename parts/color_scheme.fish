
function ticta--scheme--verb-help
  echo "Usage: ticta-scheme VERB ..."
  echo "Verbs:"
  echo "  list    show all schemes and show their color palettes"
  echo "  load-file  load specified .ini file(s) containing color schemes"
  echo "  set     "
  echo "  show    show specified or current color scheme"
  echo "  update  change a specified color of a given scheme"
  echo
  echo "Usage: ticta-scheme set [-l/--local] SCHEME"
  echo "Options:"
  echo "  -l --local  Set for only current session"
  echo
  echo "Usage: ticta-scheme update SCHEME COLOR_NAME VALUE"
end

# complete -c ticta-scheme -n "__fish_seen_subcommand_from show" -s "p" -l "pad" -r -d "Set padding for list verb"
complete -c ticta-scheme -n "__fish_seen_subcommand_from show" -s "l" -l "no-legend" -d "hide color legend at top"
complete -c ticta-scheme -n "__fish_seen_subcommand_from show" -s "n" -l "no-name" -d "hide scheme names"
complete -c ticta-scheme -n "__fish_seen_subcommand_from show" -a "$ticta_schemes"
function ticta--scheme--verb-show
  argparse 'p/pad=' 'l/no-legend' 'n/no-name' -- $argv

  set -l scheme "$argv[1]"

  if test -z "$scheme" || not contains "$scheme" $ticta_schemes
    set scheme ( ticta-cfg get color_scheme )
  end

  set -l pad_min ( math ( string length "$scheme" ) + 2 )
  if set -q _flag_no_name
    set pad_min 0
  end

  set -l pad_n "$_flag_pad"
  if not tictabox is-number "$pad_n"
    set pad_n 0
  end
  if not test "$pad_n" -ge "$pad_min" &> /dev/null
    set pad_n "$pad_min"
  end

  set -l pad ( string repeat -n "$pad_n" " " )

  set -l row_a
  set -l row_b "$pad"

  if not set -q _flag_no_name
    set row_a ( string pad --right --width "$pad_n" -- "$scheme " )
  else
    set row_a "$pad"
  end

  for color in $color_list
    set varname    "ticta_scheme_""$scheme""_""$color"
    set varname_br "ticta_scheme_""$scheme""_""br$color"

    set value    ( set_color $$varname $color )
    set value_br ( set_color $$varname_br br$color )

    set row_a "$row_a$value""███"
    set row_b "$row_b$value_br""███"
  end

  set row_a "$row_a$normal"
  set row_b "$row_b$normal"

  if not set -q _flag_no_legend
    echo "$bold$pad"" B  R  G  Y  B  M  C  W""$normal"
  end

  echo $row_a
  echo $row_b
end

function ticta--scheme--verb-list
  set -l max_len 0
  for scheme in $ticta_schemes
    set -l len ( string length "$scheme" )
    if test "$len" -gt "$max_len"
      set max_len "$len"
    end
  end
  set max_len ( math $max_len + 2 )

  set first
  for scheme in $ticta_schemes
    if set -q first
      ticta--scheme--verb-show --pad $max_len $scheme
      set -e first
    else
      ticta--scheme--verb-show --pad $max_len --no-legend $scheme
    end
    echo
  end
end

function ticta--scheme--verb-set
  argparse "l/local" -- $argv

  set name $argv[1]
  set argv $argv[2..]

  if test -z "$name" || not contains "$name" $ticta_schemes
    echo "See ticta-scheme list for more details." > /dev/stderr
    echo "Available schemes:" > /dev/stderr
    for scheme in $ticta_schemes
      echo "- "( ticta-lib format --color-block-left scheme "$scheme" > /dev/stderr )
    end
    return 1
  end

  if set -q _flag_local
    ticta-cfg set -fx color_scheme "$name"
  else
    ticta-cfg save -f color_scheme "$name"
  end

  set -gx ticta__current_scheme $name
  ticta-sync-colors
end

function ticta--scheme--verb-update
  set scheme $argv[1]
  set name $argv[2]
  set value $argv[3]

  set -g ticta_scheme_{$scheme}
  if not contains "$scheme" $ticta_schemes
    set -ga ticta_schemes "$scheme"
    complete -c ticta-scheme -n "__fish_seen_subcommand_from set" -a "$scheme"
  end

  if not contains "$name" $full_color_list
    ticta--warn "unknown color name: '$name'"
    return 1
  end

  set value ( ticta--resolve-color --agnostic "$value" )
  set -g ticta_scheme_{$scheme}_{$name} "$value"
end

function ticta--scheme--verb-load-file
  ticta--debug "loading scheme files: '$argv'"
  
  set state_section
  for line in ( cat $argv | string replace --regex ';.*' '' )
    if test -z "$line"
      continue
    end

    string match --quiet --regex '^(?:\[(?<section>\w+)\])|(?:(?<key>.+?)\s*=\s*(?<value>[.\s\S]+))$' -- "$line" 

    if test -n "$section"
      set state_section "$section"
    else if test -n "$key"
      ticta--scheme--verb-update $state_section $key $value
    else
      ticta--warn "invalid input: '$line'"
    end
  end
end


begin
  set verbs list set show
  set descriptions \
    'show all schemes' \
    'show a specific scheme or your current selected scheme' \

  complete -c ticta-scheme -f -d "Manage ticta.fish color scheme"
  for i in ( seq ( count $verbs ) )
    set verb $verbs[$i]
    set desc $descriptions[$i]
    
    complete -c ticta-scheme -n "not __fish_seen_subcommand_from $verbs" -a "$verb" -d "$desc"
  end
end
function ticta-scheme
  set verb $argv[1]
  set argv $argv[2..]

  switch "$verb"
    case "create"
      ticta--scheme--verb-create $argv
    case "load-file"
      ticta--scheme--verb-load-file $argv
    case "list"
      ticta--scheme--verb-list $argv
    case "show"
      ticta--scheme--verb-show $argv
    case "set"
      ticta--scheme--verb-set $argv
    case "help"
      ticta--scheme--verb-help $argv
    case "*"
      ticta--scheme--verb-help $argv > /dev/stderr
      return 1
  end
end
# TODO: Completions
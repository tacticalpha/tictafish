ticta--source "$ticta__dir/parts/color_lists.fish"
ticta--source "$ticta__dir/parts/color_sync.fish"

set -g normal ( set_color normal )
set -g bg_normal \e\[49m

set -g reset $normal$bg_normal

set -g ticta_schemes
set -gx ticta__current_scheme ( ticta-cfg get color_scheme )

# Resolve a color into a value that can be passed to set_color
complete -x -c ticta--resolve-color
function ticta--resolve-color
  argparse 'agnostic' 'set' 'background' 'scheme=' 'h/hex-prefix=' -- $argv
  
  set input ( string trim -- "$argv")
  
  set value

  set varname "ticta__named_color_$input"
  set varname_override "tcfg_c_$input"
  set varname_scheme "ticta_scheme_""$_flag_scheme""_$input"
  set varname_current_scheme "ticta_scheme_""$tcfg_color_scheme""_$input"

  if set -q _flag_scheme
    set value "$$varname_scheme"
  
  else if test "$input" = "normal"
    set value "normal"

  else if not set -q _flag_agnosic && set -q $varname_override && test -n "$$varname_override"
    set value "$$varname_override"

  else if not set -q _flag_agnosic && set -q $varname_current_scheme && test -n "$$varname_current_scheme"
    set value "$$varname_current_scheme"

  else if not set -q _flag_agnosic && set -q $varname && test -n "$$varname"
    set value "$$varname"

  else if contains "$input" $full_color_list
    set value "$input"

  else if string match --quiet --regex -i 'hsl\((?<hue>\d+(?:\.\d+)?)\s*,\s*(?<sat>\d+(?:\.\d+)?)%\s*,\s*(?<light>\d+(?:\.\d+)?)%\)' -- "$input"
    # Thanks https://en.wikipedia.org/wiki/HSL_and_HSV#Color_conversion_formulae
    # This is a pretty expensive calculation. It could probably be optimized a lot more.
    set -l H ( math "$hue % 360" )
    set -l S ( math "$sat/100" )
    set -l L ( math "$light/100" )  

    # chroma
    set -l C ( math "(1 - abs(2*$L - 1)) * $S" )

    # H' (H prime)
    set -l H1 ( math "$H/60" )
    # max
    set -l X ( math "$C * (1 - abs(($H1 % 2) - 1))" )

    if test "$H1" -ge 5
      set R "$C"
      set G "0"
      set B "$X"
    else if test "$H1" -ge 4
      set R "$X"
      set G "0"
      set B "$C"
    else if test "$H1" -ge 3
      set R "0"
      set G "$X"
      set B "$C"
    else if test "$H1" -ge 2
      set R "0"
      set G "$C"
      set B "$X"
    else if test "$H1" -ge 1
      set R "$X"
      set G "$C"
      set B "0"
    else if test "$H1" -ge 0
      set R "$C"
      set G "$X"
      set B "0"
    end

    # min
    set -l m ( math "$L - ($C/2)")

    # min and max clamp these between 0 and 255
    set R ( math --scale=0 "min(255, max(0, round(($R + $m) * 255)))")
    set G ( math --scale=0 "min(255, max(0, round(($G + $m) * 255)))")
    set B ( math --scale=0 "min(255, max(0, round(($B + $m) * 255)))")

    set value ( printf '#%02x%02x%02x' $R $G $B )
  else if set hex ( string match --regex --ignore-case --groups-only '^(?:#|0x)?([0-9a-f]{3,6})$' -- "$input" )
    set value "#$hex"
  else
    ticta--warn "unknown color or format: '$input'"
    set value "$normal"
  end

  if set -q _flag_set
    if test -z "$value"
      echo -n "$normal"
      return 1
    end

    if set -q _flag_background
      set_color --background "$value"
    else
      set_color "$value"
    end
  else
    if test -z "$value"
      echo -n "normal"
      return 1  
    end
    echo -n "$value"
  end
end


ticta--debug loading color schemes
source $ticta__dir/parts/color_scheme.fish
ticta-scheme load-file "$ticta__dir/parts/color_schemes.ini"
ticta-sync-colors
# set -g tprompt_c_user ( _set_color ( ticta-cfg get c_prompt_user ))

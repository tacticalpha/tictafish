
function ticta--fun-splash
  not set -q splash_time_scale; and set -l splash_time_scale 1
  not set -q splash_target_framerate; and set -l splash_target_framerate 60

  set -al steps "Bootloader initializing,375" \
  "Loading shared libraries,100" \
  "Loading user configuration,100" \
  # "Verifying configuration integrity,75" \
  "Loading user packages,75" \
  "Verifying package integrity,50" \
  "Connecting to human interface devices,100" \
  # "Probing configuation,100" \
  "Connecting to system socket,50" \
  # "Handshaking,50" \
  "Exchanging keys,50" \
  "Verifying User Identity,75" \
  "Loading terminal interface,300"

  set steps_n ( count $steps )

  esc cursor-mode-on
  esc invisible
  clear

  # set decoration_version "ticOS v"( random 1 7).( random 0 20).( random 0 99)
  set decoration_version "ticOS v"( uname -r | perl -p -e 's/(\d+\.\d+\.\d+).*/\1/' )
  set decoration_signiture "sig 0x"( uname -a | sha1sum | string sub -l ( math ( string length $decoration_version ) - 6 ) )

  set term_width 0
  set term_height 0

  set frame_time
  set pad
  set x
  set w
  set y
  set y1

  function splash-clear-line -a line
    echo ( esc goto 2 $line )( string repeat -n ( math ( tput cols ) - 2 ) " " )
  end

  set stage 0

  set step_i 0
  set step_start_time 0
  set step_time 0
  set step 0 0
  while true
    set -l frame_start_time ( now-ms )

    if test $step_i -eq 0 -o $term_width -ne ( tput cols ) -o $term_height -ne ( tput lines )
      set term_width ( tput cols )
      set term_height ( tput lines )

      set frame_time ( math "1000 / $splash_target_framerate" )
      set pad 0.2
      set x ( math "round($term_width * $pad)" )
      set w ( math "round($term_width * (1 - ($pad * 2)))" )
      set y ( math "round($term_height * 0.5) - 1" )
      set y1 ( math $y + 1 )

      set decorations ( esc goto 2 1 )$decoration_version
      set -a decorations ( esc goto 2 2 )$decoration_signiture

      # set decorations ( esc goto 3 2 )$decotation_version
      # set -a decorations ( esc goto 3 3 )$decoration_signiture
      # set -a decorations ( esc home )"┌"( string repeat -n ( math $term_width - 2) "─")"┐"
      # set -a decorations ( string repeat -n ( math $term_height - 1) "\n"( esc setcolumn $term_width )"│" )
      # set -a decorations ( esc home )( string repeat -n ( math $term_height - 1) "\n│" )
      # set -a decorations \n\r"└"( string repeat -n ( math $term_width - 2) "─")"┘"

      clear
      echo -en ( string join "" $decorations )

    end

    switch $stage
    case 0
      if test $step_time -gt $step[2]
        # Go to next step
        set step_i ( math $step_i + 1 )
        sleep ( math "0.01 * $splash_time_scale" )
        # sleep ( math "($step[2] / 20 / 1000) * $splash_time_scale" )
        if test $step_i -gt $steps_n
           # sleep ( math "0.3 * $splash_time_scale" )
           set stage ( math $stage + 1 )
           continue
         end

        set step ( string split ',' $steps[$step_i] )
        set step[2] ( math "$step[2] * $splash_time_scale" )

        splash-clear-line $y1
        esc goto $x $y1

        splash-clear-line $y
        esc goto $x $y

        set -l str $step[1]
        if test ( string length $str ) -gt $w
          set str ( string sub -s 1 -l ( math "$w - 3" ) $str )...
        end
        echo -en $str

        # sleep ( math "($step[2] / 20 / 1000) * $splash_time_scale" )
        sleep ( math "0.01 * $splash_time_scale" )
        set step_start_time ( now-ms )
      end

      set step_time ( math $frame_start_time - $step_start_time )
      if test $step_i -gt 0 -a $step_time -gt 0
        # Draw progress bar
        set -l prog ( math "$step_time / $step[2]" )
        test $prog -gt 1; and set prog 1

        set -l prog_str ( string repeat -n ( math "floor($w * $prog)" ) " " )
        esc goto $x $y1
        echo -en $invert$prog_str$normal
      end

    case 1
      splash-clear-line $y
      esc goto $x $y
      # esc clearline
      echo -en "[ Terminal ready! ]"
      sleep ( math "0.5 * $splash_time_scale" )

      set stage ( math $stage + 1 )

    case '*'
      break
    end

    set -l elapsed ( math ( now-ms ) - $frame_start_time )
    set -l sleep_time ( math "floor($frame_time - $elapsed)" )

    test $sleep_time -gt 0; and sleep ( math $sleep_time / 1000 )
  end

  functions --erase splash-clear-line

  esc cursor-mode-off
  esc visible
end

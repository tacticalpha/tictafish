
set -g ticta__motd_integrations
function ticta--load-motd
  for path in $argv
    if test -d "$path"
      set files "$path/"*
    else if test -e "$path"
      set files "$path"
    else
      return 1
    end
    
    for fn in $files
      set name ( basename $fn .fish )

      source $fn

      set check_func ticta--motd-check--$name
      if functions -q "$check_func" && not $check_func
        continue
      end
      
      set -a ticta__motd_integrations "$name"
    end
  end
end

function ticta--motd
  set lists list_{info,session,notice,issue,critical,unknown}

  set list_info     # version info, system info, etc
  set list_session  # tmux sessions, active processes
  set list_notice   # non-issue notices, like an update
  set list_issue    # warnings
  set list_critical
  set list_unknown

  set disabled_motd_parts ( ticta-cfg get disabled_motd_parts )

  for name in $ticta__motd_integrations
    if contains $name $disabled_motd_parts
      continue
    end

    set func_name "ticta--motd--$name"
    
    set output ( $func_name )
    set res "$status"

    if test ( count $output ) -lt 2
      set level "level not set"
      set content $output
    else
      set level $output[1]
      set content $output[2..]
    end

    if test "$res" -ne 0 -o -z "$content"
      continue
    end

    switch $level
      case "info"
        set prefix ""
      case "session"
        set prefix ""
      case "notice"
        set prefix "$dim$brblue""- "
      case "issue"
        set prefix "$bryellow""! "
      case "critical"
        set prefix "$bold$brred""! "
      case "*"
        set prefix "$dim$level"": " 
    end


    set list "list_$level"
    if not set -q $list
      set list list_unknown
    end

    set pad ( string repeat -n ( string-clean-len "$prefix" ) " " )

    set -a $list "$prefix$normal$content[1]"
    for line in $content[2..]
      set -a $list "$pad$normal$line"
    end
    # set -a $list ""
  end

  set all_lists $list_info $list_session $list_notice $list_issue $list_critical $list_unknown

  if test ( count $all_lists ) -eq 0
    return
  end

  for list in $lists
    if test ( count $$list ) -gt 0
      echo
    end
    
    for line in $$list
      echo "$line"
    end
  end
end

ticta--load-motd \
  $ticta__dir/parts/motd/ \
  $HOME/.config/fish/ticta/motd.d


function ticta--motd--tmux
  set -l sessions ( tmux list-sessions 2> /dev/null | count  )
  
  if test -z "$sessions" -o "$sessions" -eq 0
    return
  end

  echo "session"
  
  if test "$sessions" -eq 1
    echo ( ticta-lib format number $sessions )" tmux session active"
  else
    echo ( ticta-lib format number $sessions )" tmux sessions active"
  end
end

function ticta--motd-check--tmux
  if not command -q tmux
    return 1
  end
end
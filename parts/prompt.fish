source $ticta__dir/parts/prompt_integrations.fish
source $ticta__dir/parts/prompt_list.fish
source $ticta__dir/parts/prompt_pwd.fish

set -g ticta_prompts
function ticta--load-prompts
  for path in $argv
    if test -d "$path"
      set files "$path/"*
    else if test -e "$path"
      set files "$path"
    else
      return 1
    end

    for fn in $files
      set name ( basename $fn .fish )

      source $fn

      if functions -q "ticta--prompts-$name"
        set -a ticta_prompts $name
      end
    end
  end
end

function _ticta-available-prompts
  echo Available Prompts:
  echo -n $bold
  for prompt in $ticta_prompts
    echo "  "$prompt
  end
end

complete -x -c ticta-prompt-use -a "$ticta_prompts"
complete -x -c ticta-prompt-use -s l -l local -d "Set only for current instance"
function ticta-prompt-use -d "Set global or local prompt"
  argparse "l/local" -- $argv
  echo $argv | read name argv

  if test -z $name
    echo "No prompt provided :("
    echo
    echo "Usage:"
    echo "  ticta-prompt-use prompt [-l/--local]"
    echo "  ex: ticta-prompt-use -l minimal"
    echo
    echo "View prompts with `ticta-prompt-list`"
    _ticta-available-prompts
    return 1
  end

  set valid 0
  for prompt in $ticta_prompts
    if [ $prompt = $name ]
      set valid 1
      break
    end
  end

  if [ $valid != 1 ]
    echo "Invalid prompt"
    echo
    echo "View prompts with `ticta-prompt-list`"
    _ticta-available-prompts
    return 1
  end

  if test -n "$_flag_local"
    set -gx tcfg_prompt $name
  else
    set -ge tcfg_prompt
    set -U tcfg_prompt $name
  end
  set -gx ticta__current_prompt $name

  return 0
end

set -gx ticta__current_prompt ( ticta-cfg get prompt )
# Just calculate these once, to save a few cycles when displaying the prompt
if not set -q ticta__prompt_hostname
  # set -g ticta__prompt_hostname (hostname|cut -d . -f 1)
  set -g ticta__prompt_hostname ( ticta-get-hostname )
end

if not set -q ticta__prompt_char
  switch (id -u)
  case 0
     set -g ticta__prompt_char $brred'#'$reset
   case '*'
     #	set -g __fish_prompt_char 'λ'
    set -g ticta__prompt_char '$'$reset
  end
end

# Configure __fish_git_prompt
set -g __fish_git_prompt_char_stateseparator ' '
set -g __fish_git_prompt_color cyan
set -g __fish_git_prompt_color_flags df5f00
set -g __fish_git_prompt_color_prefix brcyan
set -g __fish_git_prompt_color_suffix brcyan
set -g __fish_git_prompt_showdirtystate true
set -g __fish_git_prompt_showuntrackedfiles true
set -g __fish_git_prompt_showstashstate true
set -g __fish_git_prompt_show_informative_status true


complete -x -c ticta--prompt
function ticta--prompt -d "Show prompt based on \$ticta__current_prompt"
  set current_user (whoami)

  set -l integrations
  if ticta-cfg assert --invert enable_prompt_integrations
  else if set -q integrations_list
    set integrations $integrations_list
  else
    set integrations ( ticta--prompt-get-integrations )
  end

  set func_name ticta--prompts-$ticta__current_prompt

  if not set -q last_status
    set last_status 0
  end
  
  last_status=$last_status \
  current_user=$current_user \
  nvm_installed=$nvm_installed \
  nvm_system=$nvm_system \
  all_integrations=$ticta__prompt_integrations \
  integrations=$integrations \
  split_integration='echo $integration | string trim | read -d \x01 name state default_output' \
  $func_name

  return $status
end

ticta--load-prompts \
  $ticta__dir/parts/prompt/ \
  $HOME/.config/fish/ticta/prompt.d
  
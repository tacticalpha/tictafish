
function ticta--prompts-default
  if test "$USE_USER_COLORS" = "1"
    set color_username $color_accent
    set color_hostname $color_accent
    set color_pwd_dir $color_accent
    set color_pwd_seperator $color_tertiary
  else
    set color_username $brmagenta
    set color_hostname $yellow
    set color_pwd_dir $brgreen
    set color_pwd_seperator $green
  end

  set -l width ( tput cols )

  set -a parts $color_username$current_user$white' at '$color_hostname$ticta__prompt_hostname
  set -a parts $white'in'$brgreen ( ticta--prompt-pwd --color-dir $color_pwd_dir --color-seperator $color_pwd_seperator )
  if test "$last_status" != "0"
    set -a parts "$white$bg_red [$last_status] "
  end

  for integration in $integrations
    eval $split_integration
    switch $name
    case '*'
      set -a parts $normal$default_output
    end
  end

  set -l cur_y 2
  echo -n $white"╭─"
  for part in $parts
    if [ -z $part ]
      continue
    end

    set -l part_len (string length (echo $part | sed 's/\x1b\[[0-9;]*m//g'))

    if [ $cur_y -ne 2 -a ( expr $cur_y + $part_len + 1 ) -ge $width ]
      # echo "too long :( "( expr $cur_y + $part_len )", "$width
      echo
      echo -n $white'│ '
      set cur_y 2

      if [ ( expr 3 + $part_len ) -gt $width ]
        set -l new_part ''
        set -l target_len ( expr $width - 3 - 2 )
        set -l n 0
        for c in ( string match -a -r "(?:\\e\[[0-9]+m)|(?:.)" $part )
          if test (string length $c ) -eq 1
            set n ( expr $n + 1 )
          end

          if [ $n -ge $target_len ]
            break
          end
          set new_part $new_part$c
        end
        set part $new_part...
      end
    end

    # echo $cur_y:+$part_len:$part" "
    echo -n $part$normal" "

    set cur_y (expr $cur_y + $part_len)
  end

  echo
  echo -n $white'╰─'$ticta__prompt_char $normal
end

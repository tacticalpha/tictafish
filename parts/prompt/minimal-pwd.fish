
function ticta--prompts-minimal-pwd
  echo ( ticta--prompt-pwd --color-dir $color_accent --color-seperator $color_tertiary )$normal
  if [ $last_status != 0 ]
    # echo -n $bg_red $last_status" "$bg_normal$red"🭬 "$normal
    echo -n $brred$bold$last_status$normal" "
  end
  [ ( id -u ) = 0 ]; and echo -n "# "; or echo -n "> "
end

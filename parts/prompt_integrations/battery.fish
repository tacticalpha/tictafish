
function ticta--prompt-integration-check-battery
  if type -q termux-battery-status
    return 0
  else if test -e "/sys/class/power_supply"
    set supplies ( find /sys/class/power_supply -maxdepth 1 -iname 'BAT?' )
    if test ( count $supplies ) = 0
      return 1
    end

    set -g ticta__system_batteries $supplies
    return 0
  end

  return 1
end

function ticta--prompt-integration-battery
  if type -q termux-battery-status
    set state ( timeout 2s termux-battery-status | grep percentage | perl -pe 's/\D*(\d+).*/\1/' )
    set state ( math "$state / 100" )
  else
    for supply in $ticta__system_batteries
      set -a state ( math ( cat $supply/energy_now ) / ( cat $supply/energy_full ))
    end
  end

  set n ( count $state )
  for level in $state 
    set -a formatted_levels ( math "round($level * 100)" )"%"
  end

  echo $state
  echo -n "$bryellow""[$brgreen$formatted_levels$bryellow]"
end

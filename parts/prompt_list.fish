
function ticta--prompt-list-names
  for name in $ticta_prompts
    echo $name
  end
end

function ticta--prompt-list-simple
  set arg "$argv[1]"
  if [ ( string length $arg ) -eq 0 ]
    read -P "Hit enter to view with current accent color ($bg_color_accent    $bg_normal) or type in a new one: " accent
    read -P "Hit enter to use current tertiary color ($bg_color_tertiary    $bg_normal) or type in a new one: " tertiary
  else
    set accent   ""
    set tertiary ""
  end


  if [ -n $accent ]; or [ -n $tertiary ]
    set -l cmd
    if [ -n $accent ]
      set -a cmd "set -g tcfg_color_accent $accent"
    end
    if [ -n $tertiary ]
      set -a cmd "set -g tcfg_color_tertiary $tertiary"
    end

    set -a cmd "ticta-reload; ticta-prompt-list current"
    fish -c ( string join "; " $cmd )
    return
  end

  set f ( mktemp -t ticta_prompt_list_output.XXXXXX )
  # echo -ne \n\n\n >> $f
  echo -e "Current accent color:   "$bg_color_accent"    "$bg_normal >> $f
  echo -e "Current tertiary color: "$bg_color_tertiary"    "$bg_normal\n\n >> $f
  for prompt in $ticta_prompts
    echo $black$bg_white $prompt ( set_color normal ) >> $f
    [ $prompt = 'block' ]; and echo -ne \n >> $f
    prompt_demo=1 ticta__current_prompt=$prompt last_status=0 ticta--prompt >> $f
    echo -ne \n >> $f
    [ $prompt = 'block' ]; and echo -ne \n >> $f
    prompt_demo=1 ticta__current_prompt=$prompt last_status=1 ticta--prompt >> $f
    echo >> $f
    echo $bg_normal$normal >> $f
    echo >> $f
  end
  echo -ne \n\n >> $f
  cat $f | less -r
end

function ticta--prompt-list-interactive

  set prompt_n ( count $ticta_prompts )
  set prompt_sel 1
  set prompt_name
  set color_sel 1
  set named_color_sel

  set edit_mode 0

  set accent_names accent tertiary
  set accent ( ticta-cfg get color_accent )
  set tertiary ( ticta-cfg get color_tertiary )

  set original_accent $accent
  set original_tertiary $tertiary

  set -a key_list 'q:quit' \
    'left/right:select prompt' \
    'up/down:select between colors' \
    's:swap colors' \
    'enter:edit color'

  set -a key_list_edit_mode 'esc/q/enter:confirm' \
    'left/right:cycle between named colors'

  set -l current_prompt ( ticta-cfg get prompt )
  for i in ( seq 1 $prompt_n )
    if test $current_prompt = $ticta_prompts[$i]
      set prompt_sel $i
      break
    end
  end

  function sync-accents -S
    ticta-cfg set color_accent $accent
    ticta-cfg set color_tertiary $tertiary
    ticta-sync-colors --just-accents
  end

  set integrations ( ticta--prompt-get-integrations )
  set last_char ""
  function draw -S
    set -l current_key_list
    if test $edit_mode -eq 1
      set current_key_list $key_list_edit_mode
    else
      set current_key_list $key_list
    end

    set out ""
    set -a out ( clear )
    set -a out ( esc home )$invert( string repeat -n ( tput cols ) ' ' )( esc home )
    set -a out $bold( string join ", $bold" $current_key_list | string replace --all ':' "$unbold " )
    set -a out $normal"\n\r"

    for i in 1 2
      set -l name $accent_names[$i]
      set -l bg_var "bg_color_$name"

      set -a out ( test $edit_mode -eq 0 -a $color_sel -eq $i; and echo -n ' >'; or echo -n '  ')
      set -a out "$name color"( test $i -eq 1; and echo "  "; or echo "" )":"

      set -a out ( test $edit_mode -eq 1 -a $color_sel -eq $i; and echo -n '>'; or echo -n ' ' )
      set -a out "$$bg_var   $bg_normal $$name"
      set -a out ( test $edit_mode -eq 1 -a $color_sel -eq $i; and echo -n '<'; or echo -n '' )
      set -a out "\n"
    end

    set prompt_name $ticta_prompts[$prompt_sel]

    set -a out "[$prompt_sel / $prompt_n] $bold$prompt_name\n\n"
    set -l prompt_func_name ticta--prompts-$prompt_name
    for line in (
      prompt_demo=1 \
      last_status=1 \
      ticta__current_prompt=$prompt_name \
      integrations_list=$integrations \
      ticta--prompt
    )
      set -a out $line"\n"
    end

    # set -a out ( esc goto 1 ( tput lines ) )"last char: "( string escape $last_char )"       "
    set -a out ( esc home )

    echo -en ( string join '' $out )
  end

  esc cursor-mode-on
  esc invisible
  clear

  stty -icanon -echo
  set char
  draw
  while set char ( dd if=/dev/stdin bs=64 count=1 2> /dev/null )
    set -l redraw 1
    if test $edit_mode -eq 1
      switch $char
      case 'q' \e ''
        set edit_mode 0
      case \e\[D \e\[C
        if test $char = \e\[C
          incr named_color_sel
        else
          decr named_color_sel
        end
        test $named_color_sel -lt 1; and set named_color_sel ( count $full_color_list )
        test $named_color_sel -gt ( count $full_color_list ); and set named_color_sel 1

        if test $color_sel -eq 1
          set accent $full_color_list[$named_color_sel]
        else
          set tertiary $full_color_list[$named_color_sel]
        end
        sync-accents
      end
    else
      switch $char
      case ''
        set edit_mode 1
        set -l selected_color
        if test $color_sel -eq 1
          set selected_color $accent
        else
          set selected_color $tertiary
        end

        for i in ( seq 1 ( count $full_color_list ) )
          if test $selected_color = $full_color_list[$i]
            set named_color_sel $i
            break
          end
        end
      case \e\[A
        test $color_sel -eq 2; and set color_sel 1
      case \e\[B
        test $color_sel -eq 1; and set color_sel 2
      case 's'
        set -l _accent $accent
        set accent $tertiary
        set tertiary $_accent
        sync-accents
      case 'q' \e
        break
      case \e\[C \e\[D
        if test $char = \e\[C
          incr prompt_sel
        else
          decr prompt_sel
        end

        test $prompt_sel -lt 1; and set prompt_sel $prompt_n
        test $prompt_sel -gt $prompt_n; and set prompt_sel 1
      case '*'
        set redraw 1
      end
    end
    set last_char $char
    test $redraw -eq 1; and draw
  end

  stty icanon echo

  esc cursor-mode-off
  esc visible


  if test ( ticta-cfg get prompt ) != $prompt_name \
    -o ( ticta-cfg get color_accent) != $accent \
    -o ( ticta-cfg get color_tertiary) != tertiary;
    and read -P 'Save selected configuration? (y/[n]) ' choice; and contains ( string lower $choice ) y yes
    ticta-prompt-use $prompt_name
    ticta-cfg save color_accent $accent
    ticta-cfg save color_tertiary $tertiary

    echo 'Saved!'
  else
    ticta-cfg set color_accent $original_accent
    ticta-cfg set color_tertiary $original_tertiary
  end
end

complete -x -c ticta-prompt-list -k -a 'interactive list names'
function ticta-prompt-list -a mode -d "List and show all available prompts"
  switch $mode
  case 'names'
    ticta--prompt-list-names
    return
  case 'list'
    ticta--prompt-list-simple
    return
  case '' 'interactive'
    ticta--prompt-list-interactive
    return
  end

  echo "usage: ticta-prompt-list [names|list|interactive]"
end

set -l i 0
for name in bold dim italic underline blinking "" invert invisible strikethrough
  set i ( math "$i + 1" )
  test -z "$name"; and continue

  set -l name_off $name"_off"

  set -g $name ( printf "\\033["$i"m" )
  if test "$i" -le 2
    set -g $name_off ( printf "\\033[22m" )
  else
    set -g $name_off ( printf "\\033[2"$i"m" )
  end

  set -g "un$name" $$name_off

end

## how 2 install
Run this inside fish:
```fish
curl https://gitlab.com/tictaccc/tictafish/-/raw/main/scripts/internal/install.fish | source && ticta-install
```

after it's cloned and added to config.fish just restart your terminal or run `fish` and you'll see the new prompt n stuff :)


## how 2 use
type `ticta-` and then tab with tictafish to see a list of all the different config functions
(ones that have two dashes, like `ticta--prompt`, are private functions that you probably dont need to touch)\
(like you can if u want but idk why you'd want to)\
(listen im not here to tell you how to live your life do whatever you want)\
(just idk why you'd wanna do that)

## Features _(wip)_
###### Configuration
Most of these features can be toggled or modified using the `ticta-cfg` command.
###### Prompt Integrations
Prompt integrations for some popular tools like git, nvm, python's venv, the unix screen tool, and android termux.
###### Shell integrations
Some QOL improvements, like creating commonly used `ls` aliases like `la` and `ll` and using `exa` if available and helper libraries that provide functions like `regex-rename` and `read-buffer`.
###### More usable window titles
Window titles that include things like the user and hostname to make working with multiple hosts easier
###### Color schemes
Selectable with `ticta-scheme-use`
###### Multiple prompts
Selectable with `ticta-prompt-list`, an interactive prompt and accent color selector

## how 2 fix if it gets borked
Running `ticta-updater revert` (or `$HOME/.config/fish/tictafish/scripts/updater.fish revert` if it's /really/ borked) will revert you to your last installed commit. Then create an issue and I'll fix it or fix it and create a pull request :)

if its **really really** borked you can always just remove the `source $HOME/.config/fish/tictafish/ticta.fish` from your `config.fish` (and then definitely create an issue pls)

#!/usr/bin/fish

function ticta-install
  if not status is-interactive; or not status is-interactive-job-control
    echo "It looks like you're running this script in a non-interactive shell"
    echo -n "It has to ask you a few questions along the way to make sure the "
    echo "install goes smoothly."
    echo
    echo -n "If you think this is a mistake, try downloading this file and running that "
    echo " (source FILENAME; ticta-install)"
    exit 1
  end

  if not type -q git
    echo "It looks like git isn't installed :("
    echo "git is required to install and update tictafish"
    echo "Install it and try again :)"
    exit 1
  end

  if test ( tput bold 2> /dev/null )
    set bold ( tput bold )
  else
    set bold ""
  end
  set normal ( set_color normal )

  read -P "Clone tictafish into "( echo ~/.config/fish/tictafish/ )"? ([Y]/n) " choice
  if test -z "$choice" -o "$choice" = "Y" -o "$choice" = "y"
    echo -n 'Cloning...'

    cd ~/.config/fish/
    set out ( mktemp -t ticta_install_output.XXXXXX )
    if not git clone https://gitlab.com/tacticalpha/tictafish.git/ > $out 2>&1
      echo \r$bold( set_color red )'Error!    '$normal
      echo "Couldn't clone tictafish :("
      cat $out
      exit 1
    else
      echo \r$bold( set_color green )'Cloned!   '$normal
    end
  end
  echo

  read -P "Append tictafish to "( echo ~/.config/fish/config.fish )"? ([Y]/n) " choice
  if test -z "$choice" -o "$choice" = "Y" -o "$choice" = "y"
    echo -n 'Writing...'

    echo -e '\nsource $HOME/.config/fish/tictafish/ticta.fish' >> $HOME/.config/fish/config.fish
    echo \r$bold( set_color green )'Written!   '
    echo
  end

  read -P "Install fisher (https://github.com/jorgebucaran/fisher)? ([Y]/n) " choice
  if test -z "$choice" -o "$choice" = "Y" -o "$choice" = "y"
    echo "curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher"
    curl -sL https://git.io/fisher | source && fisher install jorgebucaran/fisher
    echo

    read -P "install jorgebucaran/nvm.fish with fisher? ([Y]/n) " choice
    if test -z "$choice" -o "$choice" = "Y" -o "$choice" = "y"
      fisher install jorgebucaran/nvm.fish
    end
  end
end

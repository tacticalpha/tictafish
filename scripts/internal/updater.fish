# TODO: Assert if using real home
# TODO: Parse options and implement self-updating with -u


if not set -q ticta__dir
  echo "Error: \$ticta__dir isnt set, this was run without ticta.fish being initialized first"
  echo "Either run it after sourcing ticta.fish or manually set \$ticta__dir (usually ~/.config/fish/tictafish)"
  exit 1
end

function write_last_check
  set -U ticta__updater_last_check ( date +"%s" )
  set -U ticta__updater_up_to_date ( is_updated; and echo 1; or echo 0 )
  if test $ticta__updater_up_to_date -eq 1
    set -U ticta__updater_auto_notified 0
  end
end

function verb--get-commit 
  git rev-parse HEAD
end

function verb--get-remote-commit
  if set commit ( git rev-parse origin/( verb--get-release ) 2> /dev/null )
    echo $commit
  else
    return $status
  end
end

function verb--get-release
  git rev-parse --abbrev-ref HEAD
end

function verb--set-release
  set release $argv[1]

  git checkout $release
  git pull
end

function is_updated
  set local  ( verb--get-commit )
  set remote ( verb--get-remote-commit )
  or return 0

  test "$local" = "$remote"
end


cd $ticta__dir

set verb $argv[1]
set argv $argv[2..]

switch $verb
  case "is_updated"
    is_updated

  case "get-commit"
    verb--get-commit $argv

  case "get-remote-commit"
    verb--get-remote-commit $argv
    
  case "get-release"
    verb--get-release $argv

  case "set-release"
    verb--set-release $argv

  case "fetch"
    git fetch -q &> /dev/null
    write_last_check

  case "check"
    echo "Getting latest version from git..."
    echo -n $brblack
    git fetch
    echo -n $reset
    write_last_check
    if is_updated
      echo "Up to date! :)"
    else
      set ticta__update_notified 0
      # ticta--update-notify
    end

  case "check-auto"
    git fetch -q > /dev/null
    write_last_check

  case "run" "update"
    echo "Getting latest version from git..."
    git fetch
    write_last_check
    if is_updated
      echo "Already up to date!"
    else
      echo "Pulling latest version..."
      git pull
      write_last_check

      set -U ticta__updated 1

      echo "Done! enter a new fish shell with `fish` to use the newest version :)"
      echo
      echo "If anything goes wrong, you can run this script directly and revert"
      echo "to the previously installed version with "
      echo "`fish -N \"$ticta__dir/scripts/internal/updater.fish revert\"`"
      echo
    end

  case "revert"
    echo "This will run `git reset --hard HEAD@{1}` and could delete any local"\
        "changes you've made to the repo without warning."
    echo

    read -P "Continue? y/[n] " choice
    if [ (string lower $choice) = 'y' ]
      git reset --hard HEAD@{1}

      if test $tcfg_enable_auto_update_warning != "0"
        echo "ticta config setting `enable_auto_update_warning` is set to "\
            "false, set to true to prevent from automatically re-updating to "\
            "broken commit?"

        read -P "y/[n] " choice
        if [ (string lower $choice) = 'y' ]
          set -U tcfg_enable_auto_update_warning 1
        end
      end
    else
      echo "Canceling."
      exit 1
    end

  case "help" "*"
    echo "ticta.fish updater script"
    echo
    echo "Usage:"
    echo "  ticta-updater/updater.fish [options] verb"
    echo
    # echo "Options:"
    # echo "  -u, --update Fetches the latest version of updater.fish before"
    # echo "               running the verb"
    # echo "  -o, --output Writes it to a new file"
    echo
    echo "Verbs:"
    echo "  is_updated          returns current state without fetching latest git state"
    echo "  get-commit          get the current local commit"
    echo "  get-remote-commit   get the current local commit"
    echo "  get-release         get current release channel"
    echo "  set-release         set release channel (main/testing/dev)"
    echo "  fetch               run `git fetch` without echoing anything"
    echo "  check               run `git fetch` then echo if local install is up to date or not"
    echo "  run/update          get the latest state and then install it"
    echo "  revert              run `git reset --hard HEAD@{1}` to go to last git version before last pull"
    # echo "  self-update fetches the latest version of updater.fish"
end

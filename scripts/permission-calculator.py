#!/usr/bin/python3

import sys

import epysh.stdin_reader as reader
import epysh.esc as esc

STR_READ  = 'read '
STR_WRITE = 'write'
STR_EXEC  = 'exec '

col = (
    'read ',
    'write',
    'exec '
)

col_special = (
    'setuid    ',
    'setguid   ',
    'sticky bit'
)

def main():
    x = 0
    y = 0

    cols = (
        ['special'],
        ['user'],
        ['group'],
        ['others']
    )

    cols = [ l + [False]*3 for l in cols ]

    def draw():
        out = ''
        res = [0, 0, 0, 0]
        for j in range(0, 3):
            for i in range(0, 4):
                # out` += esc.color(112 if cols[i][j + 1] else 160, bg=True)
                # out += esc.color.get(28 if cols[i][j + 1] else 52, bg=True)
                selected = x == i and y == j
                out += '\033[7m[' if selected else ' '
                if i == 0:
                    out += f' {col_special[j]}'
                else:
                    out += f' {col[j]}'

                out += f' {1 if cols[i][j+1] else 0} '
                out += ']\033[27m' if selected else ' '

            out += f'{esc.color.normal}\r\n'

        for i in range(0, 4):
            if cols[i][1]: res[i] += 0o4
            if cols[i][2]: res[i] += 0o2
            if cols[i][3]: res[i] += 0o1

        out += f'\n  \033[1m{"".join([str(x) for x in res])}{esc.color.normal}'
        out += f'\r{esc.cursor.up(4)}'
        sys.stdout.write(out)

    def cleanup():
        sys.stdout.write(f'{esc.cursor.up(1)}\033[0J')


    reader.start()
    try:
        # print("  " + ", ".join([ c[0] for c in cols ]) + '\033[0K')
        print("\033[1m  special         owner      group      other\033[21m")
        sys.stdout.write('\033[?25l')
        draw()
        while True:
            # sys.stdout.write(f'\r({x}, {y})')
            try:
                res = reader.pull_and_parse()
            except KeyboardInterrupt:
                cleanup()
                break

            if res:
                res = res[0]

            key = res.name
            if key in ['\n', ' ']:
                cols[x][y + 1] = not cols[x][y + 1]
            elif key in ['q', 'esc']:
                cleanup()
                break
            elif key == 'up' and y > 0:
                y -= 1
            elif key == 'down' and y < 2:
                y += 1
            elif key == 'left' and x > 0:
                x -= 1
            elif key == 'right' and x < 3:
                x += 1

            draw()
    finally:
        reader.stop()
        sys.stdout.write('\033[?25h')

if __name__ == '__main__':
    main()

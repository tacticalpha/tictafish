#!/usr/bin/perl

use Time::HiRes qw(sleep);

# @spinner_list = ('|', '/', '-', '\\');
@spinner_list = ('.', '|', '\'', '|');
$spinner_n    = $#spinner_list + 1;

$hspinner_size = 6;

$clear_line = `tput el`;

sub color {
  my $color = $_[0];

  if ($color) {
    return "\033[38;5;" . $color . "m";
  } else {
    return "\033[39m"
  }
}

sub move {
  my $dir;
  my $mode = $_[0];
  my $n    = $_[1];

  if ($mode eq 'up') {
    $dir = 'A';
  } elsif ($mode eq 'right') {
    $dir = 'C';
  } elsif ($mode eq 'down') {
    $dir = 'B';
  } elsif ($mode eq 'left') {
    $dir = 'D';
  }
  print "\033[" . $n . $dir;
}

$c_red = color(160);

@bars=(
  -999, $c_red     . '▂',
  -80,  color(208) . '▃',
  -70,  color(226) . '▅',
  -60,  color(156) . '▆',
  -45,  color(118) . '▇',
);

$increase_str = color(118) . '▲';
$decrease_str = color(160) . '▼';
$same_str     = color(32) . '·';

$SIG{INT} = \&trap_int;
sub trap_int {
  # print "\n" x ($#interface_names + 2);
  print "\n";
  system 'tput cvvis';
  exit
}


# @matches = `iwconfig 2>&1` =~ /(?:^|\n\n)([a-zA-Z0-9]+)([\s\S]*?)\n\n/g;
#
# print "matches: " . @matches . "\n";

system 'tput civis';

@interface_names;
%vals_current;
%vals_last;
$name_Size = 0;

$i = -1;
while (true) {
  sleep 0.2;
  $i += 1;

  $iwstate = `iwconfig 2>&1`;
  @split   = split(/\n *\n/g, $iwstate);

  move('up', @interface_names + 0) if (($i != 0) and (@interface_names != 0));

  # Update values
  # while ($iwstate =~ /(?:^|\n *\n)([a-zA-Z0-9]+)(?!.*no wireless|.*ESSID:off|.*Link Quality=0)[\s\S]*?Signal level=(-[0-9]{1,3}) dBm/g) {
  #   my $interface = $0;
  #   my $strength  = $1;
  for $i (0..$#split) {
    my @match = $split[$i] =~ /([a-zA-Z0-9]+)(?!.*no wireless|.*ESSID:off|.*Link Quality=0)[\s\S]*?Signal level=(-[0-9]{1,3}) dBm/;
    next if (@match == 0);

    my $interface = $match[0];
    my $strength  = $match[1];


    if (not exists $vals_current{$interface}) {
      push(@interface_names, ($interface));
      my $len = length($interface);
      if($len > $name_size) {
        $name_size = $len;
        $hspinner_size = ($len + 10) * 1;
      }
      print "\n" if ($i == 0);
    }
    $vals_current{$interface} = $strength;
  }

  # last;

  if ($i == 0) {
    print "\n" if ($interface_names == 0);
    next
  }

  # Render

  # my $spinner_char = $spinner_list[$i % $spinner_n];
  # print
  #   "\r"
  #   . color()
  #   . $spinner_char
  #   . " " x ($name_size + 9)
  #   . $spinner_char;

  my $hspinner_end = $hspinner_size * 2;
  my $hspinner_pos = ($i % ($hspinner_end));
  print
    "\r"
    . $clear_line
    . color()
    ;
    # . $hspinner_size
    # . "\r";

  move('right', int($hspinner_pos / 2)) if ($hspinner_pos > 1);

  # if ($hspinner_pos == 0) {
  #   print "▘";
  # } els
  if ($hspinner_pos == ($hspinner_end) - 1) {
    print "▝\r▘";
  } elsif ($hspinner_pos % 2 == 1) {
    print "▝▘";
  } else {
    print "▀";
  }

  # next if ($interface_names == 0);

  for $interface (@interface_names) {
    my $strength      = $vals_current{$interface};
    my $last_strength = $vals_last{$interface};

    print
      "\n\r"
      . $clear_line
      . color()
      . $interface
      . " " x ($name_size - length($interface) + 1);

    if ($strength == false) {
      print $c_red . "\033[1m" . "!!" . color() . 'dropped' . $c_red  . "\033[1m" . '!!' . "\033[21m";
      next;
    }
    print $strength;

    if ($strength == $last_strength) {
      print $same_str;
    } elsif ($strength > $last_strength) {
      print $increase_str;
    } else {
      print $decrease_str;
    }
    print " ";

    for (my $i = 0; $i <= 9; $i += 2) {
      last if ($strength <= $bars[$i]);

      print $bars[$i + 1];
    }
  }

  for $interface (@interface_names) {
    $vals_last{$interface}    = $vals_current{$interface};
    $vals_current{$interface} = false;
  }

  # last if ($i > 2000);
  # last;
}

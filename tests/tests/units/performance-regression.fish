
register-test "performance regression check" \
  --show-output \
  --expect-status 0 \
  '
    if not set -q ARTIFACT_DIR
      echo "ARTIFACT_DIR not set"
      exit 10
    end

    mkdir "$ARTIFACT_DIR/profiles"
    
    set output ( $ticta__dir/debug regression-test --profile-dir "$ARTIFACT_DIR/profiles" --threshold "-10" )
    string join \n -- $output
    string join \n -- $output > $ARTIFACT_DIR/regression-test.yaml
'